ActiveRecord::Schema.define(version: 2019_03_16_082245) do
  create_table "product", force: :cascade do |t|
    t.string "SKU"
    t.string "model_name"
    t.string "design_name"
    t.string "theme"
    t.string "main_image"
    t.string "image_1"
    t.string "image_2"
    t.string "image_3"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
