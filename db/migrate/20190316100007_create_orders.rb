class CreateOrders < ActiveRecord::Migration[5.2]
  def change
    create_table :orders do |t|
      t.integer :product_id
      t.string :product_sku
      t.string :status

      t.timestamps
    end
  end
end
