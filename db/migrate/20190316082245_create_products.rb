class CreateProducts < ActiveRecord::Migration[5.2]
  def change
    create_table :products do |t|
      t.string :SKU
      t.string :mode_name
      t.integer :inventory_count
      t.string :design_name
      t.string :theme
      t.string :main_image
      t.string :image_1
      t.string :image_2
      t.string :image_3
      t.timestamps null: false
    end
  end
end
