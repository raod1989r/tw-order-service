require 'sinatra/activerecord'
require './models/product.rb'

ActiveRecord::Base.establish_connection(
  :adapter => "sqlite3",
  :database  => "order-service.sqlite3",
  :pool => 5
)

list = JSON.parse(File.read('products.json'))

list.each do |product|
  Product.create({SKU: product['SKU'],
    mode_name: product['Model Name'],
    design_name: product['Design Name'],
    theme: product['Theme'],
    main_image: product['Main Image'],
    image_1: product['Image 1'],
    image_2: product['Image 2'],
    image_3: product['Image 3'],
    inventory_count: 10
    })
end
