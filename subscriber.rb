class Subscriber
    def self.subscribe
      subscription = PUBSUB_TOPIC.subscription 'ACCEPT_ORDER'

      subscriber = subscription.listen do |received_message|
        puts "Order State: #{received_message.inspect}"
        received_message.acknowledge!

        if received_message.data.eql? 'ACCEPT_ORDER'
          Order.accept_order received_message.data
        elsif received_message.data.eql? 'DECLINE_ORDER'
          Order.declined_order received_message.data
        else
          raise 'UNLISTED SUBSCRIBER'
        end

        # received_message.acknowledge!
      end

      subscriber.start
      # sleep 60
      # subscriber.stop.wait!
    end
end
