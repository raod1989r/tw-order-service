require 'sinatra'
require 'active_record'
require 'sinatra/activerecord'
require './models/product'
require './models/order'
require './subscriber'
require "google/cloud/pubsub"

configure :development do
  # set :database, 'sqlite3:///order-service.sqlite3'
  # set :database, {adapter: "sqlite3", database: "order-service.sqlite3", pool: 5}
  set :show_exceptions, true
  ActiveRecord::Base.establish_connection(adapter:  'sqlite3',
                                         database: 'order-service.sqlite3')
  PUBSUB = Google::Cloud::Pubsub.new
  PUBSUB_TOPIC = PUBSUB.topic 'team5'
end

# Subscribe events
begin
  Subscriber.subscribe
rescue => e
  puts e
end

class OrderServiceApp < Sinatra::Base
  post '/order' do
    product_sku = params[:product_sku]

    if Product.stock_vailable?(product_sku)
      order = Order.create({product_sku: product_sku})

      PUBSUB_TOPIC.publish "ORDER_CREATED", order.to_json
      puts "Order created with ID #{order.id}"

      render json: {status: 200, message: 'success'}
    else
      puts "Not found product #{product_sku}"
      render json: {status: 422, message: 'failure'}
    end
  end

  get '/service-name' do
    'order service v1'
  end
end
