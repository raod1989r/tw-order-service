class Order < ActiveRecord::Base
  belongs_to :product

  class << self
    def accept_order order
      order = find(sku: order.SKU)
      order.status = 'accepted'
      product.inventory_count
      product.save && order.save
    end

    def declined_order order
      order = find(sku: order.SKU)
      order.status = 'declined'
      order.save
    end
  end
end
