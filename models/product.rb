class Product < ActiveRecord::Base

  def stock_vailable? sku
    product = find(SKU: sku)
    product.inventory_count > 0
  end
end
